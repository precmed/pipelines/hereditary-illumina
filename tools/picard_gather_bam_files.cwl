$namespaces:
  sbg: 'https://www.sevenbridges.com'
id: picard-gather-bam-files
label: picard-gather-bam-files
class: CommandLineTool
cwlVersion: v1.0
hints:
#- $import: envvar-global.yml
- $import: picard-docker.yml
- class: InlineJavascriptRequirement

inputs:
  java_arg:
    type: string
    default: "-Xmx2g"
    inputBinding:
      position: 1

  input:
    type: File
    inputBinding:
      position: 4
      prefix: "INPUT="
    doc: |
      The BAM or SAM file to sort. Required
      
  output_name:
    type: string
    inputBinding:
      position: 4
      prefix: "OUTPUT="
    doc: |
      The BAM or SAM file to sort. Required
      
outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

  index:
    type: File
    outputBinding:
      glob: $(inputs.output_name.split('.')[0].concat(".bai"))

arguments:

- valueFrom: "/usr/local/bin/picard.jar"
  position: 2
  prefix: "-jar"

- valueFrom: "GatherBamFiles"
  position: 3
  
- valueFrom: "true"
  position: 5
  separate: false
  prefix: CREATE_INDEX=

baseCommand: ["java"]

doc: ''
