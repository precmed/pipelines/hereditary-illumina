curl -v "http://83.212.73.203:31344/api/workflows/v1"\
      -F workflowUrl=http://gitlab.precmed.iit.demokritos.gr/despina/germline/raw/bae3da347991359c7ad589ff8fb8c3720592c3a7/workflows/GATK/GATK-Sub-Workflow-Workflow-h3abionet-haplotype.cwl \
      -F workflowInputs=@Germline_test.yml \
      -F workflowType=CWL \
      -F workflowTypeVersion=v1.0

curl -v "http://83.212.73.203:31344/api/workflows/v1"\
      -F workflowUrl=http://gitlab.precmed.iit.demokritos.gr/pipelines/demo/raw/master/GermlinePipeline/workflows/GATK/GATK-Sub-Workflow_haplotype_BWA.cwl \
      -F workflowInputs=@Germline_test.yml \
      -F workflowType=CWL \
      -F workflowTypeVersion=v1.0

#Second cluster
curl -v "http://83.212.74.21:30315/api/workflows/v1"\
      -F workflowUrl=http://gitlab.precmed.iit.demokritos.gr/pipelines/demo/raw/master/GermlinePipeline/workflows/GATK/GATK-Sub-Workflow_haplotype_BWA.cwl \
      -F workflowInputs=@Germline_test.yml \
      -F workflowType=CWL \
      -F workflowTypeVersion=v1.0
