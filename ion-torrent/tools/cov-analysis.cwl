#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
id: coverage-analysis

hints:
  - class: DockerRequirement
    dockerPull: sgsfak/tmap-tvc

inputs:
  - id: reference
    type: File
    inputBinding:
      position: 0
    label: Reference genome
    doc: FASTA file containing reference genome
    secondaryFiles:
      - .fai
      - .tmap.anno
      - .tmap.bwt
      - .tmap.pac
      - .tmap.sa
  - id: bed_file
    type: File
    inputBinding:
      position: 1
  - id: aligned_bam
    type: File
    inputBinding:
      position: 2
    label: Input BAM file
    doc: Input BAM file(s) containing aligned reads.
    secondaryFiles:
      - .bai

baseCommand: [coverage_report.sh]

outputs:
  - id: coverage_report
    type: File
    outputBinding:
      glob: report.html
    doc: An HTML document with coverage details

