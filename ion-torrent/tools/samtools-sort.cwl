class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: samtools_sort
baseCommand:
  - samtools
  - sort
inputs:
  - id: input_bam
    type: File
    inputBinding:
      position: 2
    label: Input bam file.
outputs:
  - id: sorted_bam
    doc: The sorted BAM file
    type: File
    outputBinding:
      glob: $(inputs.input_bam.nameroot)_sorted.bam
arguments:
  - position: 0
    prefix: '-m'
    valueFrom: '1000M'
  - position: 0
    prefix: '-l'
    valueFrom: '1'
  - position: 0
    prefix: '-@'
    valueFrom: '12'
  - position: 0
    prefix: '-o'
    valueFrom: $(inputs.input_bam.nameroot)_sorted.bam
  - position: 0
    prefix: '-O'
    valueFrom: bam
  - position: 0
    prefix: '-T'
    valueFrom: $(inputs.input_bam.nameroot)
hints:
  - class: DockerRequirement
    dockerPull: sgsfak/tmap-tvc
requirements:
  - class: InlineJavascriptRequirement
