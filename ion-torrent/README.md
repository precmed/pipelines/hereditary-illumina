# CWL Tools and Workflows for the Ion Torrent pipeline

This is a repository for the development of the 'Ion Torrent' preferred pipeline as CWL workflow. It's still work in progress...

We are using the [iontorrent/variantcaller](https://hub.docker.com/r/iontorrent/variantcaller) Docker image (based on Ubuntu 14.04) that contains the needed commnand line tools (e.g. `tmap`, `samtools`, etc). The current list of tools that have been described in CWL are in the `tools` folder and are the following:

* `tmap`, for the mapping of reads on a reference genome. This tool given an "unaligned" BAM, produces an "aligned" one.
* `samtools index`, for indexing an aligned BAM file
* `variant_caller`, for variant calling, which produces the final VCF file(s)

The pipeline itself is described in CWL and can be found in `workflows/torrent-pipeline.cwl`. It consists of the sequential execution of `tmap`, `samtools index`, and `variant_caller_pipeline.py`. The following image shows a visual representation of it created by [Rabix](http://rabix.io):

![Ion Torrent pipeline](images/torrent-pipeline.jpg)

## Notes

* `tmap` requires the reference genome to be indexed. In addition to having a `.fai` reference index (generated by `samtools faidx genome.fa`), you should also do a `tmap index -f genome.fa` and this command produces files with extensions `.tmap.sa`, `.tmap.pac`, `.tmap.bwt`, and `.tmap.anno`
